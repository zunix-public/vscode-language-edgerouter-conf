# Vyatta/Edgerouter configuration file - VSCode Extension

An extension for vyatta / edgerouter `.boot` configuration file, with :

-   Color syntax
-   Formatter
-   Templating

# Formatter and colors

![demo](images/demo-formatter.gif)

## Usage

Edit `.boot` or `.erconf` configuration file. It's all!

# Templating

![demo](images/demo-template.gif)

## Usage

TODO

# Hex chain generator

![demo](images/stringToHex.gif)

Generate rfc 3118 auth hex from string. CMD + SHIFT + P and select `string to rfc3118-auth hex`.

# Extension Life

## Known Issues

-   Not all keywords added yed...

## Release Notes

### 0.1.2

-   Convert rfc 3118 auth hex generator to hex generator

### 0.1.1

-   Add rfc 3118 auth hex string generator

### 0.0.9

-   Resolve bug of indentation of generated file

### 0.0.8

-   Update doc

### 0.0.7

-   Add templating support

### 0.0.6

-   Rename extension name
-   Refactor

### 0.0.5

-   Rename extension name
-   Add icon

### 0.0.4

-   Cancel formatter if `{` count is not equals with `}` count
-   Add default `.boot` file usage
-   Add `global-option` keywork
-   Add `masquerade` keywork
-   Add `bonding` keywork
-   add `hash-policy` keywork
-   add `layer2` keywork
-   add `802.3ad` keywork
-   add `bond-group` keywork
-   add `analytics-handler` keywork
-   add `crash-handler` keywork
-   add `send-analytics-report` keywork
-   add `send-crash-report` keywork
-   add `notice` keywork
-   add `debug` keywork

### 0.0.3

-   Update Readme.md

### 0.0.2

-   `registerDocumentFormattingEditProvider` usage instead of editing disk files

### 0.0.1

-   Color syntax
-   Indentation by "manual" file rewrite
-   add `drop` keywork
-   add `accept` keywork
-   add `enable` keywork
-   add `disable` keywork
-   add `true` keywork
-   add `false` keywork
-   add `autos+` keywork
-   add `all` keywork
-   add `icmp` keywork
-   add `tcp` keywork
-   add `udp` keywork
-   add `tcp_udp` keywork
-   add `autoconf` keywork
-   add `update` keywork
-   add `dhcp` keywork
-   add `masquerade` keywork
-   add `layer2` keywork
-   add `802.3ad` keywork
-   add `notice` keywork
-   add `debug` keywork
-   add `default-action` keywork
-   add `local-zone` keywork
-   add `enable-default-log` keywork
-   add `description ` keywork
-   add `action ` keywork
-   add `log ` keywork
-   add `name ` keywork
-   add `interface ` keywork
-   add `established ` keywork
-   add `related ` keywork
-   add `invalid ` keywork
-   add `address-group ` keywork
-   add `duplex` keywork
-   add `speed` keywork
-   add `mtu` keywork
-   add `address` keywork
-   add `all-ping` keywork
-   add `broadcast-ping` keywork
-   add `send-redirects` keywork
-   add `ipv6-receive-redirects` keywork
-   add `ipv6-src-route` keywork
-   add `receive-redirects` keywork
-   add `ip-src-route` keywork
-   add `source-validation` keywork
-   add `syn-cookies` keywork
-   add `log-martians` keywork
-   add `protocol[-]` keywork
-   add `new` keywork
-   add `port?!-` keywork
-   add `client-option` keywork
-   add `egress-qos` keywork
-   add `dup-addr-detect-transmits` keywork
-   add `default-route-distance` keywork
-   add `name-server` keywork
-   add `default-route` keywork
-   add `default-router` keywork
-   add `wan-interface` keywork
-   add `original-port` keywork
-   add `lan-interface` keywork
-   add `hairpin-nat` keywork
-   add `auto-firewall-nat-exclude` keywork
-   add `auto-firewall` keywork
-   add `global-option` keywork
-   add `bonding` keywork
-   add `bond-group` keywork
-   add `send-analytics-report` keywork
-   add `send-crash-report` keywork
-   add `hash-policy` keywork
-   add `global-parameters` keywork
-   add `disabled` keywork
-   add `hostfile-update` keywork
-   add `authoritative` keywork
-   add `host-name` keywork
-   add `mode` keywork
-   add `pre-shared-secret` keywork
-   add `ike-lifetime` keywork
-   add `server-1` keywork
-   add `server-2` keywork
-   add `nat-traversal` keywork
-   add `override-hostname-ip` keywork
-   add `inet` keywork
-   add `alias` keywork
-   add `encrypted-password` keywork
-   add `plaintext-password` keywork
-   add `public-keys` keywork
-   add `key` keywork
-   add `alias` keywork
-   add `full-name` keywork
-   add `level` keywork
-   add `hwnat` keywork
-   add `forwarding` keywork
-   add `vlan` keywork
-   add `level` keywork
-   add `dpi` keywork
-   add `export` keywork
-   add `components` keywork
-   add `distribution` keywork
-   add `password` keywork
-   add `url` keywork
-   add `time-zone` keywork
-   add `unifi-controller` keywork
-   add `lease` keywork
-   add `stop` keywork
-   add `domain-name` keywork
-   add `ip-address` keywork
-   add `mac-address` keywork
-   add `ntp-server` keywork
-   add `use-dnsmasq` keywork
-   add `cache-size` keywork
-   add `listen-on` keywork
-   add `older-ciphers` keywork
-   add `outbound-interface` keywork
-   add `type` keywork
-   add `allow-root` keywork
-   add `protocol-version` keywork
-   add `http-port` keywork
-   add `https-port` keywork
-   add `dhcp-interface` keywork
-   add `{|}` keywork
-   add `name` keywork
-   add `port-group` keywork
-   add `rule` keywork
-   add `port-forward` keywork
-   add `forward-to` keywork
-   add `state` keywork
-   add `firewall` keywork
-   add `vif` keywork
-   add `ethernet` keywork
-   add `loopback` keywork
-   add `interfaces` keywork
-   add `zone-policy` keywork
-   add `destination` keywork
-   add `source` keywork
-   add `group` keywork
-   add `dhcp-options` keywork
-   add `ipv6` keywork
-   add `zone` keywork
-   add `from` keywork
-   add `service` keywork
-   add `unms` keywork
-   add `dhcp-server` keywork
-   add `mdns` keywork
-   add `dns` keywork
-   add `forwarding` keywork
-   add `repeater` keywork
-   add `gui` keywork
-   add `nat` keywork
-   add `ip` keywork
-   add `static-host-mapping` keywork
-   add `login` keywork
-   add `ntp` keywork
-   add `offload` keywork
-   add `ipv4` keywork
-   add `syslog` keywork
-   add `global` keywork
-   add `traffic-analysis` keywork
-   add `analytics-handler` keywork
-   add `crash-handler` keywork
-   add `authentication` keywork
-   add `vpn` keywork
-   add `nat-networks` keywork
-   add `l2tp` keywork
-   add `remote-access ` keywork
-   add `local-users` keywork
-   add `client-ip-pool` keywork
-   add `package` keywork
-   add `ssh` keywork
-   add `shared-network-name` keywork
-   add `allowed-network` keywork
-   add `subnet` keywork
-   add `static-mapping` keywork
-   add `server` keywork
-   add `facility` keywork
-   add `repository` keywork
-   add `port-group` keywork
