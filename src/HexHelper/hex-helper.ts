export class HexHelper {
    public getHexFromString(rawString: string): string {
        return Array.from(rawString)
            .map((x) => this.charToHex(x))
            .join(":");
    }

    private charToHex(char: string): string {
        return char.charCodeAt(0).toString(16).toUpperCase().padStart(2, "0");
    }
}
