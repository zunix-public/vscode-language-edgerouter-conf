import * as vscode from "vscode";
import { HexHelper } from "./hex-helper";

export class StringToHexVsCodeCommand {
    public async runCommand() {
        const userInput = await this.getUserInput();
        if (userInput === undefined) return;

        const calculatedHex = new HexHelper().getHexFromString(userInput);
        this.sendResultToUser(calculatedHex);
    }

    private async getUserInput() {
        return await vscode.window.showInputBox({
            placeHolder: "",
            prompt: "Enter string to convert in rfc3118-auth compatible hex chain",
        });
    }

    private sendResultToUser(calculatedHex: string) {
        vscode.window.activeTextEditor?.edit((edit) => {
            var cursorPos = vscode.window.activeTextEditor?.selection?.start;
            if (cursorPos != null) edit.insert(cursorPos, calculatedHex);
        });
    }
}
