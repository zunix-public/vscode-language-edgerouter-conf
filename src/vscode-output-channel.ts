import * as vscode from "vscode";
import { TextMessages } from "./text-messages";

export class VsCodeOutputChannel {
    private context: vscode.ExtensionContext;
    private static channel: vscode.OutputChannel = vscode.window.createOutputChannel(TextMessages.OUTPUT_CHANNEL_NAME);

    /**
     * Self called static constrcutor
     */
    private static _initialize = (() => {
        VsCodeOutputChannel.appendLineToChannel(TextMessages.EXTENTION_LOADED);
    })();

    private static appendLineToChannel(line: string) {
        VsCodeOutputChannel.channel.appendLine("> " + line);
    }

    constructor(context: vscode.ExtensionContext) {
        this.context = context;
        context.subscriptions.push(VsCodeOutputChannel.channel);
    }

    protected writeLog(message: string) {
        VsCodeOutputChannel.appendLineToChannel(message);
    }

    protected showStatusMessage(message: string) {
        let disposable = vscode.window.setStatusBarMessage(message, 1000);
        this.context.subscriptions.push(disposable);
    }

    protected showError(error: string, exception: any) {
        vscode.window.showErrorMessage(error);
        console.error(error + " : " + exception);
    }

    protected showInfo(info: string) {
        vscode.window.showInformationMessage(info);
        console.info(info);
    }
}
