import { IndentationModel } from "./indentation-model";
import { TextMessages } from "../text-messages";

export class ErconfFormatter {
    private TAB_SPACE_COUNT: number = 4;
    private TAB_SPACE: string = " ";
    private STRING_EMPTY: string = "";

    public formatText(docText: string, eolString: string = "\n"): string {
        if (!this.isDocumentValid(docText)) {
            const errorText = TextMessages.ERROR_INVALID_BRACKETS_COUNT;
            throw new Error(errorText);
        }

        const indentedLines: Array<IndentationModel> = this.getIndentationForText(docText);
        return this.generateIndentedText(indentedLines, eolString);
    }

    private isDocumentValid(docText: string): boolean {
        return docText.split("{").length - 1 == docText.split("}").length - 1;
    }

    private getIndentationForText(text: string): Array<IndentationModel> {
        let indentationLevel = 0;
        let intentationLines: Array<IndentationModel> = new Array<IndentationModel>();
        const documentLines = text.split(/\r?\n/);
        documentLines.forEach((fileLine) => {
            if (fileLine.includes("}")) indentationLevel--;

            intentationLines.push(new IndentationModel(indentationLevel, fileLine.trim()));

            if (fileLine.includes("{")) indentationLevel++;
        });

        return intentationLines;
    }

    private generateIndentedText(identedLines: Array<IndentationModel>, eolString: string): string {
        return identedLines
            .map((indentationLine) => {
                return this.STRING_EMPTY.padEnd(indentationLine.IndentationLevel * this.TAB_SPACE_COUNT, this.TAB_SPACE) + indentationLine.LineContent;
            })
            .join(eolString);
    }
}
