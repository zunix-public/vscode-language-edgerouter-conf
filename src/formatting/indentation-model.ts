export class IndentationModel {
    public constructor(indentationLevel: number, lineContent: string) {
        this.IndentationLevel = indentationLevel;
        this.LineContent = lineContent;
    }

    public IndentationLevel: number = 0;
    public LineContent: string = "";
}
