import * as vscode from "vscode";
import { TextMessages } from "../text-messages";
import { VsCodeOutputChannel } from "../vscode-output-channel";
import { ErconfFormatter } from "../formatting/base-formatter";

export class FormatErconfFile extends VsCodeOutputChannel {

    constructor(context: vscode.ExtensionContext) {
        super(context);
    }

    public executeFormat(document: vscode.TextDocument): vscode.TextEdit[] {
        try {
            super.writeLog(`${TextMessages.FORMATTING_STATUS_MESSAGE} ${document.fileName}`);
            super.showStatusMessage(`${TextMessages.FORMATTING_STATUS_MESSAGE} ...`);

            const eolString: string = this.getEol(document);
            const docText = document.getText();
            const formatter = new ErconfFormatter();
            const formattedText = formatter.formatText(docText, eolString);

            const textRange = this.getFullDocumentTextRange(document);
            return [vscode.TextEdit.replace(textRange, formattedText)];
        } catch (ex: any) {
            vscode.window.showErrorMessage(ex.toString());
            console.error(ex.toString());
            return [];
        }
    }

    private getEol(document: vscode.TextDocument): string {
        const cr = "\r";
        const lf = "\n";
        switch (document.eol) {
            case 1:
            default:
                return lf;
            case 2:
                return cr + lf;
        }
    }

    private getFullDocumentTextRange(document: vscode.TextDocument): vscode.Range {
        const endlineSize = document.lineAt(document.lineCount - 1).range.end.character;
        return new vscode.Range(new vscode.Position(0, 0), new vscode.Position(document.lineCount - 1, endlineSize));
    }
}
