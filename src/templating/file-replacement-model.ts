export class TagReplacementModel {
    public constructor(TagDefinition: string, tagContent: string) {
        this.TagDefinition = TagDefinition;
        this.TagContent = tagContent;
    }

    public TagDefinition: string = "";
    public TagContent: string = "";
}
