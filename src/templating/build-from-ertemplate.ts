import * as vscode from "vscode";
import * as fs from "fs";
import * as glob from "glob";
import { TextMessages } from "../text-messages";
import { VsCodeOutputChannel } from "../vscode-output-channel";
import { TagReplacementModel } from "./file-replacement-model";
import { ErconfFormatter } from "../formatting/base-formatter";

export class BuildFromErtemplate extends VsCodeOutputChannel {
    private rootProjectPath: String | undefined;

    constructor(context: vscode.ExtensionContext) {
        super(context);
        this.rootProjectPath = this.getDocumentWorkspaceFolder();
    }

    private getDocumentWorkspaceFolder(): string | undefined {
        const fileName = vscode.window.activeTextEditor?.document.fileName;
        return vscode.workspace.workspaceFolders?.map((folder) => folder.uri.fsPath).filter((fsPath) => fileName?.startsWith(fsPath))[0];
    }

    public buildFromTemplate() {
        try {
            const templateFile = this.rootProjectPath + "/template.ertemplate";
            const outputFile = this.rootProjectPath + "/generated.boot.erconf";
            if (!fs.existsSync(templateFile)) return;

            super.writeLog(`Generating ${outputFile} from ${templateFile}`);
            const data = this.readFileContent(templateFile);
            const outputContent = this.replaceTagsWithTemplate(data);
            const formatter = new ErconfFormatter();
            const formattedText = formatter.formatText(outputContent);

            fs.writeFileSync(outputFile, formattedText);
            super.writeLog(`Generating ${outputFile} done.`);
            super.showInfo(TextMessages.EXTENSION_SHORT_NAME + " : Generation from template success");
        } catch (exception) {
            super.showError(TextMessages.EXTENSION_SHORT_NAME + " : Generation from template failed", exception);
        }
    }

    private readFileContent(filePath: string) {
        return fs.readFileSync(filePath, { encoding: "utf8", flag: "r" });
    }

    private replaceTagsWithTemplate(templateContent: string): string {
        const replacementTags = this.findFilesReplacements(templateContent);
        let outputFileContent = templateContent.slice();

        for (const replacementTag of replacementTags) {
            const replacementText = this.getContentForReplacementPattern(replacementTag.TagContent);
            outputFileContent = outputFileContent.replace(replacementTag.TagDefinition, replacementText);
        }

        return outputFileContent;
    }

    private findFilesReplacements(fullText: string): Array<TagReplacementModel> {
        const regex = new RegExp('@\\("(.*?)"\\)', "g");
        const matches = [...fullText.matchAll(regex)];
        return matches.map((match) => new TagReplacementModel(match[0], match[1]));
    }

    private getContentForReplacementPattern(rawFileReplacementPattern: string) {
        let content = "";
        const filesToIntegrate = this.getFilesForReplacementPattern(rawFileReplacementPattern);

        for (const replacementFilePath of filesToIntegrate) {
            super.writeLog(`Reading ${replacementFilePath}`);
            const replacementFile = this.readFileContent(replacementFilePath);
            content += replacementFile + "\n";
        }

        return content;
    }

    private getFilesForReplacementPattern(rawFileReplacementPattern: string): string[] {
        const absoluteFileReplacementPattern = this.convertPathToAbsolute(rawFileReplacementPattern);
        return glob.sync(absoluteFileReplacementPattern);
    }

    private convertPathToAbsolute(path: string): string {
        let absolutePath = path;
        if (path.startsWith("./")) {
            absolutePath = this.rootProjectPath + "/" + path.substring(2);
        }
        return absolutePath;
    }
}
