import * as vscode from "vscode";

export class TextMessages {
    public static readonly EXTENSION_SHORT_NAME = "Vy(atta)";
    public static readonly EXTENSION_NAME = `${this.EXTENSION_SHORT_NAME} .boot file support`;
    public static readonly OUTPUT_CHANNEL_NAME = `${this.EXTENSION_NAME}`;
    public static readonly EXTENTION_LOADED = `${this.EXTENSION_NAME} is loaded`;
    public static readonly ERROR_INVALID_BRACKETS_COUNT = "Opened brackets count != closed brackets count.";
    public static readonly FORMATTING_STATUS_MESSAGE = "Formatting";
}
