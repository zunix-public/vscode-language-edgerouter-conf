import * as vscode from "vscode";
import { TextMessages } from "./text-messages";
import { FormatErconfFile } from "./formatting/format-erconf";
import { BuildFromErtemplate } from "./templating/build-from-ertemplate";
import { StringToHexVsCodeCommand } from "./HexHelper/string-to-hex-vscode-command";

export function activate(context: vscode.ExtensionContext) {
    let extension = new FormatErconfFile(context);
    let erTemplate = new BuildFromErtemplate(context);
    let stringToHexVsCommand = new StringToHexVsCodeCommand();

    console.log(TextMessages.EXTENTION_LOADED);

    context.subscriptions.push(
        vscode.commands.registerCommand("extension.stringToHex", () => {
            stringToHexVsCommand.runCommand();
        })
    );

    context.subscriptions.push(
        vscode.workspace.onDidSaveTextDocument(() => {
            erTemplate.buildFromTemplate();
        })
    );

    vscode.languages.registerDocumentFormattingEditProvider("edgerouter", {
        provideDocumentFormattingEdits(document: vscode.TextDocument): vscode.TextEdit[] {
            return extension.executeFormat(document);
        },
    });
}
